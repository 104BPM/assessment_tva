﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using TVA_Assessment.Models;
using TVA_Assessment.Utilities;

namespace TVA_Assessment.Controllers
{
    public class PersonsController : Controller
    {
        // GET: Persons       
        public PersonsController()
        {
        }
        public ActionResult Index()
        {
            return RedirectToAction("Index", "Home");
        }

        public JsonResult GetPersons()
        {
            using (var db = new TVAEntities())
            {
                var persons = db.Persons.Select(x => new
                {
                    Id = x.code,
                    x.name,
                    x.surname,
                    x.id_number
                });

                string searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();

                if (!string.IsNullOrEmpty(searchValue))
                {
                    persons = persons.Where(m => m.name.Contains(searchValue) || m.surname.Contains(searchValue) || m.id_number.Contains(searchValue));
                }
                return Json(FormDataTables.LoadDataTable(persons, Request));
            }
        }
        [HttpGet]
        public ActionResult CRUD(int Id)
        {
            using (var db = new TVAEntities())
            {
                var person = db.Persons.Where(x => x.code == Id).FirstOrDefault();
                return View(person);
            }

        }
        [HttpPost]
        public JsonResult CRUD(Person person)
        {
            try
            {
                using (var db = new TVAEntities())
                {
                    //Update
                    if (person.code > 0)
                    {
                        var dbRecord = db.Persons.Where(x => x.code == person.code).FirstOrDefault();
                        if (dbRecord != null)
                        {
                            //compare ID numbers
                            if (dbRecord.id_number != person.id_number && UserExists(person.id_number))
                            {
                                return Json(new { Success = false, Message = $"ID Number {person.id_number} is already in use." }, JsonRequestBehavior.AllowGet);
                            }
                            dbRecord.id_number = person.id_number;
                            dbRecord.name = person.name;
                            dbRecord.surname = person.surname;
                            //db.Entry(dbRecord).CurrentValues.SetValues(person);
                            db.Entry(dbRecord).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                        }
                    }
                    //New person
                    else
                    {

                        if (UserExists(person.id_number))
                        {
                            return Json(new { Success = false, Message = $"ID Number {person.id_number} is already in use." }, JsonRequestBehavior.AllowGet);
                        }
                        db.Persons.Add(person);
                        db.SaveChanges();
                    }
                    return Json(new { Success = true,person_code= person.code }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception E)
            {
                return Json(new { Success = false, E.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult NewPerson()
        {
            return View();
        }

        public bool UserExists(string idNumber)
        {
            using (var db= new TVAEntities())
            {
                var persons = db.Persons.Where(x => x.id_number == idNumber).FirstOrDefault();
                if (persons != null)
                {
                    return true;
                }
            }
            return false;
        }
    }
}