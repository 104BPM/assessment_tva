﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TVA_Assessment.Models;

namespace TVA_Assessment.Controllers
{
    public class HomeController : Controller
    {
        public TVAEntities db { get; set; }
        public HomeController()
        {
            db = new TVAEntities();
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Persons()
        {

            return View();
        }
    }
}