﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TVA_Assessment.Models;
using TVA_Assessment.Utilities;

namespace TVA_Assessment.Controllers
{
    public class TransactionsController : Controller
    {
        public TVAEntities db { get; set; }
        // GET: Transactions
        public TransactionsController()
        {
            db = new TVAEntities();
        }

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetTransactions(int accountCode)
        {
            using (var db = new TVAEntities())
            {
                var transactions = db.Transactions.Where(x => x.account_code == accountCode).Select(x => new
                {
                    Id = x.code,
                    transaction_date = x.transaction_date.ToString(),
                    x.capture_date,
                    x.amount,
                    x.description
                });
                string searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();

                if (!string.IsNullOrEmpty(searchValue))
                {
                    transactions = transactions.Where(m => m.description.Contains(searchValue) || m.amount.ToString().Contains(searchValue) || m.transaction_date.ToString().Contains(searchValue)
                    || m.capture_date.ToString().Contains(searchValue));
                }
                return Json(FormDataTables.LoadDataTable(transactions, Request), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult GetTransaction(int transactionId)
        {
            using (var db = new TVAEntities())
            {
                var transaction = db.Transactions.Where(x => x.code == transactionId).FirstOrDefault();
                return View(transaction);
            }
        }

        public ActionResult NewTransaction(int accountId)
        {
            var newTransaction = new Transaction
            {
                account_code = accountId
            };
            return View(newTransaction);
        }

        [HttpPost]
        public JsonResult CRUD(Transaction transaction)
        {
            try
            {
                using (var db = new TVAEntities())
                {
                    if (transaction.code > 0)
                    {

                        decimal tempDb = 0;
                        transaction.capture_date = DateTime.Now;
                        var dbTransaction = db.Transactions.Where(x => x.code == transaction.code).FirstOrDefault();
                        //db.Entry(dbTransaction).CurrentValues.SetValues(transaction)
                        tempDb = dbTransaction.amount;
                        dbTransaction.amount = transaction.amount;
                        if (transaction.amount > tempDb)
                        {
                            transaction.amount =  transaction.amount - tempDb ;
                        }
                        else
                        {
                            transaction.amount = (tempDb - transaction.amount)*-1;
                            
                        }
                        dbTransaction.transaction_date = transaction.transaction_date;
                        dbTransaction.description = transaction.description;
                        db.Entry(dbTransaction).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                    else
                    {
                        transaction.capture_date = DateTime.Now;
                        db.Transactions.Add(transaction);
                        db.SaveChanges();
                    }
                    //update the outstanding balance
                    var dbAccount = db.Accounts.Where(x => x.code == transaction.account_code).FirstOrDefault();
                    if (dbAccount != null)
                    {
                        dbAccount.outstanding_balance += transaction.amount;
                        db.Entry(dbAccount).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                    return Json(new { Success = true }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception E)
            {
                return Json(new { Success = false, E.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}