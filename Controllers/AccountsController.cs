﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TVA_Assessment.Utilities;
using TVA_Assessment.Models;

namespace TVA_Assessment.Controllers
{
    public class AccountsController : Controller
    {
        // GET: Accounts
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetAccounts(int userId)
        {
            using (var db = new TVAEntities())
            {
                var accounts = db.Accounts.Where(x => x.person_code == userId).Select(x => new
                {
                    Id = x.code,
                    x.account_number,
                    x.outstanding_balance,
                    x.closed,
                    canClose = (x.outstanding_balance == 0 ? true : false)
                });

                string searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();

                if (!string.IsNullOrEmpty(searchValue))
                {
                    accounts = accounts.Where(m => m.account_number.Contains(searchValue) || m.outstanding_balance.ToString().Contains(searchValue));
                }
                return Json(FormDataTables.LoadDataTable(accounts, Request), JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetAccount(int accountId)
        {
            using (var db = new TVAEntities())
            {
                var account = db.Accounts.Where(x => x.code == accountId).FirstOrDefault();
                return View(account);
            }
        }

        public ActionResult NewAccount(int userId)
        {
            var newAccount = new Account()
            {
                person_code = userId
            };
            return View(newAccount);
        }

        public JsonResult ToggleAccount(int accountId)
        {
            try
            {
                using (var db = new TVAEntities())
                {
                    var account = db.Accounts.Where(x => x.code == accountId).FirstOrDefault();
                    if (account.outstanding_balance == 0)
                    {
                        account.closed = (account.closed != null && account.closed == true ? false : true);
                        db.Entry(account).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        return Json(new { Success = true }, JsonRequestBehavior.AllowGet);
                    }
                    return Json(new { Success = false, Message=$"Please settle the balance of {account.outstanding_balance} first." }, JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception E)
            {
                return Json(new { Success = false, E.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult CRUD(Account account)
        {
            try
            {
                using (var db = new TVAEntities())
                {
                    if (account.code > 0)
                    {
                        var dbAccount = db.Accounts.Find(account.code);
                        if (account.account_number != dbAccount.account_number && AccNoExists(account.account_number))
                        {
                            return Json(new { Success = false, Message = "" }, JsonRequestBehavior.AllowGet);
                        }
                        //db.Entry(dbAccount).CurrentValues.SetValues(account);
                        dbAccount.account_number = account.account_number;
                        dbAccount.person_code = account.person_code;
                        db.Entry(dbAccount).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                    else
                    {
                        //New
                        if (AccNoExists(account.account_number))
                        {
                            return Json(new { Success = false, Message = $"Account number {account.account_number} is already taken!" }, JsonRequestBehavior.AllowGet);
                        }
                        account.closed = false;
                        db.Accounts.Add(account);
                        db.SaveChanges();
                    }
                    return Json(new { Success = true, accountId = account.code }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception E)
            {
                return Json(new { Success = false, E.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public bool AccNoExists(string accNum)
        {
            using (var db = new TVAEntities())
            {
                var account = db.Accounts.Where(x => x.account_number == accNum).FirstOrDefault();
                if (account != null)
                {
                    return true;
                }
            }
            return false;
        }
    }
}