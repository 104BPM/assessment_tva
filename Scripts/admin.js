﻿function getPersons() {
    $('#dtPersons').dataTable({
        processing: true, // for show progress bar
        serverSide: true, // for process server side
        filter: true,
        orderMulti: false,  // for disable multiple column at once
        pageLength: 10,
        language: {
            loadingRecords: '&nbsp;',
            processing: '<div class="spinner"></div>'
        },// this is for disable filter (search box)
        ajax: {
            url: "/Persons/GetPersons",
            type: "post",
            dataType: "json",
            global: true
        },
        columns:
            [
                { data: "name", title: "First Name" },
                { data: "surname", title: "Surname" },
                { data: "id_number", title: "Id No." },
                {
                    data: "Id", visible: true, render: function (data) {
                        return "<a href='/Persons/CRUD?Id=" + data + "'>Edit</a>";
                    }
                }
            ]
    });
}

function getAccounts(userId) {
    $('#dtAccounts').dataTable({
        processing: true, // for show progress bar
        serverSide: true, // for process server side
        filter: true,
        orderMulti: false,  // for disable multiple column at once
        pageLength: 10,
        language: {
            loadingRecords: '&nbsp;',
            processing: '<div class="spinner"></div>'
        },// this is for disable filter (search box)
        ajax: {
            url: "/Accounts/GetAccounts?userid=" + userId,
            type: "post",
            dataType: "json",
            global: true
        },
        columns:
            [
                { data: "account_number", title: "Account No." },
                { data: "outstanding_balance", title: "Outstanding bal." },
                {
                    data: "closed", title: "Active", render: function (data) {
                        if (data == true) {
                            return "<p class='text-warning'>Account Closed</p>";
                        }
                        else {
                            return "<p class='text-success'>Account Active</p>";
                        }
                    }
                },
                {
                    data: "Id", visible: true, render: function (data) {
                        return "<a href='/Accounts/GetAccount?accountId=" + data + "'>View</a>";
                    }
                },
                {
                    data: "Id", visible: true, render: function (data, row)
                    {
                        return "<button onclick='toggleAccount("+data+")'>Close/Re-Open Account</button>";
                    }
                }
            ]
    });
}

function getTransactions(accountId) {
    $('#dtTransactions').dataTable({
        processing: true, // for show progress bar
        serverSide: true, // for process server side
        filter: true,
        orderMulti: false,  // for disable multiple column at once
        pageLength: 10,
        language: {
            loadingRecords: '&nbsp;',
            processing: '<div class="spinner"></div>'
        },// this is for disable filter (search box)
        ajax: {
            url: "/Transactions/GetTransactions?accountCode=" + accountId,
            type: "post",
            dataType: "json",
            global: true
        },
        columns:
            [
                { data: "transaction_date", title: "Date" },
                { data: "amount", title: "Amount" },
                { data: "description", title: "Description" },
                {
                    data: "Id", visible: true, render: function (data) {
                        return "<a href='/Transactions/GetTransaction?transactionId=" + data + "'>View</a>";
                    }
                }
            ]
    });
}

function toggleAccount(data) {
    var result = confirm('Are you sure');
    if (result == true)
    {
        $.post("/Accounts/ToggleAccount?accountId=" + data, function (data) {
            if (data.Success == true)
            {
                //refresh
                location.reload();
            }
            else {
                alert(data.Message);
            }
        });
    }

}