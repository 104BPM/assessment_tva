﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;

namespace TVA_Assessment.Utilities
{
    public static class FormDataTables
    {
        public static object LoadDataTable(IQueryable<object> Data, HttpRequestBase Request)
        {
            var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            sortColumn = (string.IsNullOrEmpty(sortColumn) ? "Id" : sortColumn);
            var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

            if (!(string.IsNullOrEmpty(sortColumn) || string.IsNullOrEmpty(sortColumnDir)))
            {
                Data = Data.OrderBy(sortColumn + " " + sortColumnDir);
            }

            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            //Paging Size (10,20,50,100)    
            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int recordsTotal = 0;

            //total number of rows count     
            recordsTotal = Data.Count();
            //Paging     
            var data = Data.Skip(skip).Take(pageSize).ToList();
            //Returning Json Data    
            return new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
        }
    }
}